# Use the official Node.js 14.x image as the base image
FROM node:14
    
# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install application dependencies
RUN npm install

# Copy the entire application code to the working directory
COPY . .

# Expose the port on which your application runs (replace <PORT> with the actual port number)
EXPOSE 5000

# Start the application
CMD ["npm", "start"]
